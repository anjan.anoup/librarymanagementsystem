package Database;

import java.util.List;

import Database.Models.Book;
import Database.Models.DataTable;
import Database.Models.Register;
import Database.Models.User;
import Database.Models.UserType;

public class DataQuery {
	private DataQuery() {
		//TODO: Init db properties
	}
	
	public final static DataQuery instance = new DataQuery();
	
	private TempDB db = new TempDB();
	
	//User Type add
	public <Item extends DataTable> void insertOrUpdateItem(Item newItem) {
		db.insertOrUpdateItem(newItem);
	}
	
	public <Item extends DataTable> Item removeItem(Item newItem) {
		return db.removeItem(newItem);
	}
	
	public <Item extends DataTable> Item removeItem(Integer removeId, Class<Item> type) {
		return db.removeItem(removeId, type);
	}
	
	public List<UserType> getAllUserTypes() {
		return db.getAllUserTypes();
	}
	
	public List<User> getAllUsers() {
		return db.getAllUsers();
	}
	
	public List<Book> getAllBooks() {
		return db.getAllBooks();
	}
	
	public List<Register> getAllRegisters() {
		return getAllRegisters();
	}
	
	
	public void createDummyData() {
		createDummyUserTypes();
		createDummyUsers();
		createDummyBooks();
	}
	
	public void createDummyUserTypes() {
		db.insertOrUpdateItem(new UserType(1, "Admin"));
		db.insertOrUpdateItem(new UserType(2, "Teacher"));
		db.insertOrUpdateItem(new UserType(3, "Student"));
	}
	
	public void createDummyUsers() {
		db.insertOrUpdateItem(new User(1, "Raquibul Hasan", 2001, "raquibul.h@samsung.com", "amiJaniNa", 1)); //Admin
		db.insertOrUpdateItem(new User(2, "Taposhi Rabeya", 2002, "taposhi.r@samsung.com", "amiJaniNa", 1)); //Admin
		db.insertOrUpdateItem(new User(3, "Pritam Sen", 3001, "pritam.sen@samsung.com", "amiJaniNa", 2)); //Teacher
		db.insertOrUpdateItem(new User(4, "Mouinul Islam", 3002, "mouinul.i@samsung.com", "amiJaniNa", 2)); //Teacher
		db.insertOrUpdateItem(new User(5, "Anjan Kumar Majumder", 4001, "anjan.m@samsung.com", "amiJani", 3)); //Student
		db.insertOrUpdateItem(new User(6, "Sumi Kumdu", 4002, "sumi.kundu@samsung.com", "amiJaniNa", 3)); //Student
	}
	
	public void createDummyBooks() {
		db.insertOrUpdateItem(new Book(1, "Harry Potter", "J.K. Rowling", 100));
		db.insertOrUpdateItem(new Book(2, "History", "Taposhi Rabeya. :p", 15));
		db.insertOrUpdateItem(new Book(3, "Vugol", "Raquibul Hasan. :D", 5));
		db.insertOrUpdateItem(new Book(4, "Grammer", "Mr. Ku", 1));
		db.insertOrUpdateItem(new Book(5, "Teach Yourself C++", "Herbert Schildtg", 2));
	}
}
