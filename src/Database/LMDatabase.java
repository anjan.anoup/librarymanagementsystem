package Database;

import java.sql.*;

import Database.Models.Book;
import Database.Models.Register;
import Database.Models.User;
import Database.Models.UserType;

public class LMDatabase {
	// Shared Instance
	public static final LMDatabase instance = new LMDatabase();
	
	
	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost:3306/?autoReconnect=true&allowPublicKeyRetrieval=true&useSSL=false";
	private static final String DB_NAME = "myLibrary";
	private static final String DB_FULL_URL = "jdbc:mysql://localhost:3306/" + DB_NAME +"?autoReconnect=true&allowPublicKeyRetrieval=true&useSSL=false";

	//  Database credentials
	private static final String USER = "root";
	private static final String PASS = "";
	
	private Connection conn = null;
	private Statement stmt = null;
	
	private final String createDBStmt = "CREATE DATABASE IF NOT EXISTS " + DB_NAME;

	private final String createUserType = "CREATE TABLE IF NOT EXISTS " + UserType.TABLE_NAME 
										+ " (id INT UNSIGNED NOT NULL AUTO_INCREMENT, "
										+ UserType.name +" varchar (20) NOT NULL UNIQUE, "
										+ "Primary Key (id))";
//										+ "UNIQUE (" + UserType.name + ")";

	private final String createBook = "CREATE TABLE IF NOT EXISTS " + Book.TABLE_NAME 
										+ " (id INT UNSIGNED NOT NULL AUTO_INCREMENT, " 
										+ Book.bookName + " varchar (20), " 
										+ Book.author + " varchar(20), " 
										+ Book.numberOfCopy + " int (5), "
										+ "Primary Key (id))";
	
	private final String createUser = "CREATE TABLE IF NOT EXISTS " + User.TABLE_NAME 
										+ " (id INT UNSIGNED NOT NULL AUTO_INCREMENT, " 
										+ User.userName + " varchar(200), " 
										+ User.userGenId + " int, phone int, " 
										+ User.email + " varchar(250), " 
										+ User.password + " varchar(70), "
										+ User.typeId + " INT UNSIGNED NOT NULL, "
										+ "Primary Key (id), "
										+ "FOREIGN KEY (" + User.typeId + ") REFERENCES " + UserType.TABLE_NAME + "(id))";

	private final String createRegisterBook = "CREATE TABLE IF NOT EXISTS " + Register.TABLE_NAME 
										+ " (id INT UNSIGNED NOT NULL AUTO_INCREMENT, " 
										+ Register.userId + " INT UNSIGNED NOT NULL, " 
										+ Register.bookId + " INT UNSIGNED NOT NULL, " 
										+ Register.issueDate + " BIGINT, " 
										+ Register.returnDate + " BIGINT, " 
										+ Register.status + " boolean, "
										+ "Primary Key (id), "
										+ "FOREIGN KEY (" + Register.userId + ") REFERENCES " + User.TABLE_NAME + " (id), "
										+ "FOREIGN KEY (" + Register.bookId + ") REFERENCES " + Book.TABLE_NAME + " (id))";
	
	private final String insertUserType = "INSERT IGNORE INTO " + UserType.TABLE_NAME + "(" + UserType.name + ") values(\"Liberian\"),(\"Stuff\"),(\"Student\")";
	
	private LMDatabase() {
		try{
			Class.forName(JDBC_DRIVER);

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
			
			stmt = conn.createStatement();
//			stmt.executeUpdate("DROP DATABASE IF EXISTS " + DB_NAME);
			stmt.executeUpdate(createDBStmt);
			stmt.close();
			conn.close();
			
			System.out.println("Getting FULL DB Connection");
			conn = DriverManager.getConnection(DB_FULL_URL, USER, PASS);
			stmt = conn.createStatement();
			System.out.println("Connected with DB");
		    
			conn.setAutoCommit(false);
			
			stmt.executeUpdate(createUserType);
			
			stmt.executeUpdate(insertUserType);
			
			stmt.executeUpdate(createBook);
			stmt.executeUpdate(createUser);
			stmt.executeUpdate(createRegisterBook);
			
			conn.commit();
			conn.setAutoCommit(true);
			
			ResultSet rs = stmt.executeQuery("Select * from " + UserType.TABLE_NAME);
			while (rs.next()) {
				System.out.println("Data Found: Id: " + rs.getInt("id") + " , Name: " + rs.getString(UserType.name));
			}
		    
		} catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
			if (conn != null) {
				try {
					System.err.print("Transaction is being rolled back");
					conn.rollback();
				} catch (SQLException excep) {
					excep.printStackTrace();
				}
			}
		} catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		} finally{
			//finally block used to close resources
			System.out.println("Database Created.");
			
//			try{
//				if(stmt!=null) {
//					conn.close();
//				}
//			} catch(SQLException se){
//			}// do nothing
//			try{
//				if (conn!=null) {
//					conn.close();
//				}
//			}catch(SQLException se){
//				se.printStackTrace();
//			}//end finally try
		}

	};
	
	
	public void finalize() {  
		try{
			if (stmt != null) {
				conn.close();
			}
		} catch(SQLException se){
			se.printStackTrace();
		}// do nothing
		try{
			if (conn != null) {
				conn.close();
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
	} 
}
