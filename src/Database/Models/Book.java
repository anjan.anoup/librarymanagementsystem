package Database.Models;

public class Book extends DataTable {
	public static final String TABLE_NAME = "Book";
	public static final String bookName = "bookName";
	public static final String author = "author";
	public static final String numberOfCopy = "numberOfCopy";
	
	private String mBookName;
	private String mAuthor;
	private Integer mNumberOfCopy;
	
	public Book(Integer id, String bookName, String author, Integer numberOfCopy) {
		super(id);
		this.mBookName = bookName;
		this.mAuthor = author;
		this.mNumberOfCopy = numberOfCopy;
	}
	
	public Integer getId() {
		return super.getId();
	}
	
	public String getBookName() {
		return mBookName;
	}
	
	public String getAuthor() {
		return mAuthor;
	}
	
	public Integer getNumberOfCopy() {
		return mNumberOfCopy;
	}
	
}
