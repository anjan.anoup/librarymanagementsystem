package Database.Models;

public class DataTable {
	private final Integer id;
	
	public DataTable(final Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
}
