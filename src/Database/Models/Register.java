package Database.Models;

public class Register extends DataTable {
	public static final String TABLE_NAME = "RegisterBook";
	public static final String userId = "userId";
	public static final String bookId = "bookId";
	public static final String issueDate = "issueDate";
	public static final String returnDate = "returnDate";
	public static final String status = "status";
	
	private Integer mUserId;
	private Integer mBookId;
	private Long mIssueDate;
	private Long mReturnDate;
	private Boolean mStatus;

	public Register(Integer id, Integer userId, Integer bookId, Long issueDate, Long returnDate, Boolean status) {
		super(id);
		this.mUserId = userId;
		this.mBookId = bookId;
		this.mIssueDate = issueDate;
		this.mReturnDate = returnDate;
		this.mStatus = status;
	}

	public Integer getId() {
		return super.getId();
	}

	public Integer getUserId() {
		return mUserId;
	}

	public Integer getBookId() {
		return mBookId;
	}

	public Long getIssueDate() {
		return mIssueDate;
	}

	public Long getReturnDate() {
		return mReturnDate;
	}
	
	public Boolean getmStatus() {
		return mStatus;
	}
}
