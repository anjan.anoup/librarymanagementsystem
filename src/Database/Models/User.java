package Database.Models;

public class User extends DataTable {
	public static final String TABLE_NAME = "User";
	public static final String userName = "userName";
	public static final String userGenId = "userGenId";
	public static final String email = "email";
	public static final String password = "password";
	public static final String typeId = "typeId";
	
	private String mUserName;
	private Integer mUserGenId;
	private String mEmail;
	private String mPassword;
	private Integer mUserTypeId;
	
	public User(Integer id, String userName, Integer userGenId, String email, String password, Integer userTypeId) {
		super(id);
		this.mUserName = userName;
		this.mUserGenId = userGenId;
		this.mEmail = email;
		this.mPassword = password;
		this.mUserTypeId = userTypeId;
	}

	public Integer getId() {
		return super.getId();
	}

	public String getUserName() {
		return mUserName;
	}
	
	public Integer getUserGenId() {
		return mUserGenId;
	}

	public String getEmail() {
		return mEmail;
	}

	public String getPassword() {
		return mPassword;
	}

	public Integer getUserTypeId() {
		return mUserTypeId;
	}
	
}
