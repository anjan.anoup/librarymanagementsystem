package Database.Models;

public class UserType extends DataTable {
	public static final String TABLE_NAME = "UserType";
	public static final String name = "name";
	
	private String mName;

	public UserType(Integer id, String name) {
		super(id);
		this.mName = name;
	}
	
	public Integer getId() {
		return super.getId();
	}

	public String getName() {
		return mName;
	}
}
