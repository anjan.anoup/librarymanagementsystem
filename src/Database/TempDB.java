package Database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Database.Models.Book;
import Database.Models.DataTable;
import Database.Models.Register;
import Database.Models.User;
import Database.Models.UserType;

class TempDB {
	private final HashMap<Integer, UserType> userTypeList;
	private final HashMap <Integer, User> userList;
	private final HashMap<Integer, Book> bookList;
	private final HashMap<Integer, Register> registerList;
	
	enum DataTables {
		USER_TYPE, USER, BOOK, REGISTER;
	}
	
	TempDB() {
		userTypeList = new HashMap<Integer, UserType>();
		userList = new HashMap<Integer, User>();
		bookList = new HashMap<Integer, Book>();
		registerList = new HashMap<Integer, Register>();
	}
	
	void clearAll() {
		clearUserTypes();
		clearBooks();
		clearUsers();
		clearRegisters();
	}
	
	void clearUserTypes() {
		userTypeList.clear();
	}
	
    void clearUsers() {
		userList.clear();
	}
	
	void clearBooks() {
		bookList.clear();
	}
	
	void clearRegisters() {
		registerList.clear();
	}
	
	<Item extends DataTable> void insertOrUpdateItem(Item newItem) {
		if (newItem instanceof UserType) {
			userTypeList.put(newItem.getId(), (UserType) newItem);
		} else if (newItem instanceof User) {
			userList.put(newItem.getId(), (User) newItem);
		} else if (newItem instanceof Book) {
			bookList.put(newItem.getId(), (Book) newItem);
		} else if (newItem instanceof Register) {
			registerList.put(newItem.getId(), (Register) newItem);
		}
	}
	
	@SuppressWarnings("unchecked")
	<Item extends DataTable> Item removeItem(Item removeItem) {
		if (removeItem instanceof UserType) {
			return (Item) userTypeList.remove(removeItem.getId());
		} else if (removeItem instanceof User) {
			return (Item) userList.remove(removeItem.getId());
		} else if (removeItem instanceof Book) {
			return (Item) bookList.remove(removeItem.getId());
		} else if (removeItem instanceof Register) {
			return (Item) registerList.remove(removeItem.getId());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	<Item extends DataTable> Item removeItem(Integer removeId, Class<Item> type) {
		if (type == UserType.class) {
			return (Item) userTypeList.remove(removeId);
		} else if (type == User.class) {
			return (Item) userList.remove(removeId);
		} else if (type == Book.class) {
			return (Item) bookList.remove(removeId);
		} else if (type == Register.class) {
			return (Item) registerList.remove(removeId);
		}
		return null;
	}
	
	List<UserType> getAllUserTypes() {
		return new ArrayList<UserType>(userTypeList.values());
	}
	
	List<User> getAllUsers() {
		return new ArrayList<User>(userList.values());
	}
	
	List<Book> getAllBooks() {
		return new ArrayList<Book>(bookList.values());
	}
	
	List<Register> getAllRegisters() {
		return new ArrayList<Register>(registerList.values());
	}
}
