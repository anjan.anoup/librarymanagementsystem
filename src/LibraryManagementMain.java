import Database.DataQuery;
import Database.LMDatabase;
import UserType.UserTypeView;

public class LibraryManagementMain {

	public static void main(String[] args) {
		// Create Database
		LMDatabase db = LMDatabase.instance;
		
		
		// Dummy Data
		DataQuery.instance.createDummyData();
		
		UserTypeView userTypeView = new UserTypeView();
		userTypeView.enterUserTypeMenu();
		
		
		System.out.println("Exit App");
		db.finalize();
	}

}
