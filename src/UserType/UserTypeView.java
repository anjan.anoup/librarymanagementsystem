package UserType;

import java.util.Scanner;

import Database.Models.UserType;

public class UserTypeView {
	private final UserTypeViewModel viewModel;
	private final Scanner scanner;
	public UserTypeView() {
		viewModel = new UserTypeViewModel();
		scanner = new Scanner(System.in);
		scanner.useDelimiter("[;\r\n]+");
	}
	
	public void enterUserTypeMenu() {
		
		while (true) {
			System.out.println("Please enter Options");
			System.out.println("1. Create new User Type");
			System.out.println("2. Show All User Type");
			System.out.println("3. Update a User Type");
			System.out.println("4. Remove a User Type");
			System.out.println("0. Return to Previous Menu");
			
			Integer input = scanner.nextInt();
			if (input == 0) {
				break;
			}
			
			switch (input) {
			case 1: 
				createNewUserType();
				break;
			case 2:
				showAllUserTypes();
				break;
			case 3:
				updateUserType();
				break;
			case 4:
				deleteUserType();
				break;
			default:
				System.out.println("Wrong Menu. Pless Enter for try again.");
				scanner.nextLine();
				break;
			}
		}
	}
	
	private void createNewUserType() {
		System.out.print("Please enter New User Type Name: ");
		String name = scanner.next();
		viewModel.addNewUserType(name);
		System.out.println("User Type is Created for: " + name);
		goToPreviousMenu();
		
	}
	
	private void showAllUserType() {
		System.out.println("Current Available User Types");
		for (UserType type: viewModel.getAllUserType()) {
			System.out.println("Type Id: " + type.getId() + ". Type Name: " + type.getName());
		}
	}
	
	private void goToPreviousMenu() {
		System.out.println("Pless Enter for previous Menu.");
		scanner.nextLine();
		scanner.nextLine();
	}
	
	private void showAllUserTypes() {
		showAllUserType();
		goToPreviousMenu();
	}
	
	private void updateUserType() {
		showAllUserType();
		System.out.print("Enter Type Name for Update: ");
		String oldTypeName = scanner.next();
		
		int oldTypeId = viewModel.getUserTypeId(oldTypeName);
		if (oldTypeId < 0) {
			System.out.println("Wrong Menu. Pless Enter To return.");
			goToPreviousMenu();
			return;
		}
		
		System.out.println("Enter New Type name for Update: ");
		String newTypeName = scanner.next();
		
		viewModel.updateUserType(oldTypeId, newTypeName);
		System.out.println("Updated User type: " + newTypeName + ".");
		goToPreviousMenu();
	}
	
	private void deleteUserType() {
		showAllUserType();
		System.out.print("Enter Type Id for Delete: ");
		Integer removeId = scanner.nextInt();
		
		if (viewModel.removeUserType(removeId)) {
			System.out.println("User Type deleted for Id: " + removeId + " is Completed.");
			goToPreviousMenu();
			return;
		}
		
		System.out.println("Wrong Id: " + removeId + " Entered.");
		goToPreviousMenu();
	}
}
