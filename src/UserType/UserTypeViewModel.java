package UserType;

import java.util.List;

import Database.DataQuery;
import Database.Models.UserType;

class UserTypeViewModel {
	private DataQuery dataQuery = DataQuery.instance;
	
	UserTypeViewModel() {}
	
	void addNewUserType(String typeName) {
		int newId = dataQuery.getAllUserTypes().size() + 1; //Do DB Right now
		dataQuery.insertOrUpdateItem(new UserType(newId, typeName));
	}
	
	void updateUserType(Integer id, String newTypeName) {
		dataQuery.insertOrUpdateItem(new UserType(id, newTypeName));
	}
	
	Boolean removeUserType(Integer id) {
		return dataQuery.removeItem(id, UserType.class) != null;
	}
	
	Integer getUserTypeId(String typeName) {
		List<UserType> allTypes = dataQuery.getAllUserTypes();
		for (UserType type: allTypes) {
			if (type.getName().equalsIgnoreCase(typeName)) {
				return type.getId();
			}
		}
		return -1;
	}
	
	List<UserType> getAllUserType() {
		return dataQuery.getAllUserTypes();
	}
}
